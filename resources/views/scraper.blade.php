@extends('master')

@section('page_title', 'Web Scraper Test')

@section('content')
<form id="form-scraper" role="form">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label>Keywords</label>
                <input type="text" class="form-control" id="keywords" name="txtKeywords" placeholder="Search keywords here">
            </div>
        </div>
    </div>
</form>

<div class="scrap-results">

</div>

@endsection
