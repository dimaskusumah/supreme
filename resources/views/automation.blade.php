@extends('master')

@section('page_title', 'Automation Test')

@section('content')
<h3>Routing</h3>
<hr>
<form id="form-automation" role="form" novalidate="novalidate">
    <div class="row mb-5">
        <div class="col-md-6">
            <h3>Origin</h3>
            <hr>
            <div class="form-group">
                <label>Country From <span class="text-red">*</span></label>
                <select class="form-control select2bs4" name="selCountryFrom" id="country-from" style="width: 100%;" required>

                </select>
            </div>
            <div class="form-group">
                <label>Postal Code <span class="text-red">*</span></label>
                <input type="number" class="form-control" id="postal-code-from" name="txtPostCodeFrom" placeholder="Postal Code" required>
            </div>
        </div>

        <div class="col-md-6">
            <h3>Destination</h3>
            <hr>
            <div class="form-group">
                <label>Country To <span class="text-red">*</span></label>
                <select class="form-control select2bs4" name="selCountryTo" id="country-to" style="width: 100%;" required>

                </select>
            </div>
            <div class="form-group">
                <label>Postal Code <span class="text-red">*</span></label>
                <input type="number" class="form-control" id="postal-code-to" name="txtPostCodeTo" placeholder="Postal Code" required>
            </div>
        </div>
    </div>

    <h3>Cargo Details</h3>
    <hr>
    <div class="row mb-5">
        <div class="col-12 mb-3">
            <h5>Package Details</h5>
        </div>
        <div class="form-group col-2">
            <label>Quantity <span class="text-red">*</span></label>
            <input type="number" class="form-control" id="quantity" name="txtQuantity" placeholder="0" required>
        </div>
        <div class="form-group col-2">
            <label>Length <span class="text-red">*</span></label>
            <input type="number" class="form-control" id="length" name="txtLength" placeholder="cm" required>
        </div>
        <div class="form-group col-2">
            <label>Width <span class="text-red">*</span></label>
            <input type="number" class="form-control" id="width" name="txtWidth" placeholder="cm" required>
        </div>
        <div class="form-group col-2">
            <label>Height <span class="text-red">*</span></label>
            <input type="number" class="form-control" id="height" name="txtHeight" placeholder="cm" required>
        </div>
        <div class="form-group col-2">
            <label>Weight <span class="text-red">*</span></label>
            <input type="number" class="form-control" id="weight" name="txtWeight" placeholder="kg" required>
        </div>
    </div>

    <h3>Current Total</h3>
    <hr>
    <div class="row">
        <div class="alert alert-info alert-dismissible col-12 d-none" id="#hint">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-info"></i> Info!</h5>
            Hi, I can't find another page after this on Kuehne+Nagel site. So apologize if I can't make it a 100% simillar like Kuehne+Nagel site.
        </div>
        <div class="form-group col-2">
            <label>Quantity</label>
            <h2><span id="quantity-sum"></span></h3>
        </div>
        <div class="form-group col-2">
            <label>Volume</label>
            <h2><span id="volume-sum"></span> &#13221;</h3>
        </div>
        <div class="form-group col-2">
            <label>Weight</label>
            <h2><span id="weight-sum"></span> kg</h3>
        </div>
        <div class="form-group col-2">
            <label>Chargeable weight</label>
            <h2><span id="charge-sum"></span> kg</h3>
        </div>
    </div>

    <div class="card-footer text-right">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>

@endsection
