<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExerciseController extends Controller
{
    // Home
    public function home(){
        return view('index');
    }

    public function automation(Request $req){
        return view('automation');
    }

    public function scraper(Request $req){
        $client = new Client();
        $tiUrl = 'https://www.ti.com/store/ti/en/search/?text=';
        $data = null;

        if(isset($req->search)){
            $crawler = Goutte::request('GET', $tiUrl . $req->search);

            $crawler->filter('.downloadable-item')->each(function ($node) {
                $results = $node->text();
            });
        }

        return view('scraper');
    }
}
