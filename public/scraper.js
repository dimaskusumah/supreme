$(function() {
    $('#form-scraper').on('keyup', function() {

        var keywords = $('[name=txtKeywords]').val();

        $.ajax({
            type: 'GET',
            url: '/web-scrap-test?search=' + keywords,
            success: function(msg) {
                if (msg[0].status == 'success') {
                    loadOver('off');
                    alertMessage(msg);
                } else {
                    loadOver('off');
                    alertMessage(msg);
                }
            },
            error: function(msg) {
                loadOver('off');
                alertMessage(msg);
            }
        });
    });

});