$(function() {

    $.getJSON('/storage/country.json').done(
        function(data) {
            var cto = $('#country-to').select2({
                theme: 'bootstrap4',
                placeholder: 'Please Select Option',
                data: data.results
            });

            var cto = $('#country-from').select2({
                theme: 'bootstrap4',
                placeholder: 'Please Select Option',
                data: data.results
            });
        }
    );

    var formValidate = $('#form-automation').validate({
        rules: {
            selCountryFrom: {
                required: true,
            },
            selCountryTo: {
                required: true,
            },
            txtPostalCodeFrom: {
                required: true,
                number: true,
                maxlength: 6
            },
            txtPostalCodeTo: {
                required: true,
                number: true,
                maxlength: 6
            },
        },
        messages: {
            selCountryFrom: {
                required: "Please select Origin Country",
            },
            selCountryTo: {
                required: "Please select Destination Country",
            },
            txtPostalCodeFrom: {
                required: "Please input origin postal code",
                maxlength: "Maximum number is 6 length",
                number: "This field can only filled by number"
            },
            txtPostalCodeTo: {
                required: "Please input origin postal code",
                maxlength: "Maximum number is 6 length",
                number: "This field can only filled by number"
            }
        },
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    $('#form-automation').on('change', function() {
        var quant = $('[name=txtQuantity]').val();
        var length = $('[name=txtLength]').val();
        var width = $('[name=txtWidth]').val();
        var height = $('[name=txtHeight]').val();
        var weight = $('[name=txtWeight]').val();

        if (quant != '' && length != '' && width != '' && height != '' && weight != '') {
            $('#hint').show();
            $('#quantity-sum').text(quant);
            var volume = (quant * (length * width * height)) / 1000000;
            var num = numeral(volume).format('0,0.000');
            $('#volume-sum').text(num);
            var totalWeight = quant * weight;
            $('#weight-sum').text(totalWeight);
            var chargeWeight = (quant * (length * width * height)) / 6000;
            var num2 = numeral(chargeWeight).format('0,0.00');
            $('#charge-sum').text(num2);

        }
    });

});